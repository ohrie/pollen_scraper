import scrapy

class pollenSpider(scrapy.Spider):
    name = "pollen"

    def start_requests(self):
        urls = ['https://www.dwd.de/DE/leistungen/gefahrenindizespollen/gefahrenindexpollen.html?view=renderJsonResults&undefined=Absenden&cl2Categories_LeistungsId=gefahrenindexpollen&lsId=463856&cl2Categories_Zeit=gefahrenindexpollen_1&cl2Categories_Typ=gefahrenindexpollenart_5&cl2Categories_Format=text']
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):
        page = response.url.split("/")[-2]
        filename = 'pollen.html'
        with open(filename, 'wb') as f:
            f.write(response.body)
        self.log('Saved file %s' % filename)
