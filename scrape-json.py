import scrapy

open('pollen.json', 'w').close()

class pollenSpider(scrapy.Spider):
    name = "pollen-json"
    start_urls = ['https://www.dwd.de/DE/leistungen/gefahrenindizespollen/gefahrenindexpollen.html?view=renderJsonResults&undefined=Absenden&cl2Categories_LeistungsId=gefahrenindexpollen&lsId=463856&cl2Categories_Zeit=gefahrenindexpollen_1&cl2Categories_Typ=gefahrenindexpollenart_5&cl2Categories_Format=text']

    custom_settings = {
        'FEED_EXPORT_ENCODING' : 'utf-8',
        'FEED_FORMAT':'json',
        'FEED_URI':'file://pollen.json',
    }

    def parse(self, response):
        for region in response.css('tr:not(:first-child):not(:last-child):not(:nth-child(2))'):
            yield {
                'name': region.css('td ::text').extract_first(),
                'intensity': region.css('td:nth-child(2)::text').extract()
            }
