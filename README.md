README
======

## Catch URL for text
When performing a click on "Text" Button, look at Browsers Network Tab and get URL from Rendered JSON.

## Start Spider
```scrapy runspider scrape-json.py  -o pollen.json```
