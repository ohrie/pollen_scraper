* Wert von lsId ist egal.
* cl2Categories_Zeit gibt die Pollenart an
* In einer <tr> ist immer eine Pollenart im Ersten <td> , im zweiten die Belastung
## URLs
### Hasel
https://www.dwd.de/DE/leistungen/gefahrenindizespollen/gefahrenindexpollen.html?view=renderJsonResults&undefined=Absenden&cl2Categories_LeistungsId=gefahrenindexpollen&lsId=463856&cl2Categories_Zeit=gefahrenindexpollen_1&cl2Categories_Typ=gefahrenindexpollenart_0&cl2Categories_Format=text

### Erle
https://www.dwd.de/DE/leistungen/gefahrenindizespollen/gefahrenindexpollen.html?view=renderJsonResults&undefined=Absenden&cl2Categories_LeistungsId=gefahrenindexpollen&lsId=4636&cl2Categories_Zeit=gefahrenindexpollen_1&cl2Categories_Typ=gefahrenindexpollenart_1&cl2Categories_Format=text

### Birke
https://www.dwd.de/DE/leistungen/gefahrenindizespollen/gefahrenindexpollen.html?view=renderJsonResults&undefined=Absenden&cl2Categories_LeistungsId=gefahrenindexpollen&lsId=463856&cl2Categories_Zeit=gefahrenindexpollen_1&cl2Categories_Typ=gefahrenindexpollenart_2&cl2Categories_Format=text

### Gräser
https://www.dwd.de/DE/leistungen/gefahrenindizespollen/gefahrenindexpollen.html?view=renderJsonResults&undefined=Absenden&cl2Categories_LeistungsId=gefahrenindexpollen&lsId=4636&cl2Categories_Zeit=gefahrenindexpollen_1&cl2Categories_Typ=gefahrenindexpollenart_1&cl2Categories_Format=text

### Beifuß 
https://www.dwd.de/DE/leistungen/gefahrenindizespollen/gefahrenindexpollen.html?view=renderJsonResults&undefined=Absenden&cl2Categories_LeistungsId=gefahrenindexpollen&lsId=463856&cl2Categories_Zeit=gefahrenindexpollen_1&cl2Categories_Typ=gefahrenindexpollenart_5&cl2Categories_Format=text
